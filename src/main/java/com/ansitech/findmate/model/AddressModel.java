package com.ansitech.findmate.model;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AddressModel implements Serializable{

	private int id;
	private String currentState;
	private String currentCity;
	private String currentTownOrArea;
	private String currentZipCode;
	private String currentAddress;
	private String nativeState;
	private String nativeCity;
	private String nativeTownOrArea;
	private String nativeZipCode;
	private String nativeAddress;
	private double nativeLatitude;
	private double nativeLongitude;
	
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the currentState
	 */
	public String getCurrentState() {
		return currentState;
	}


	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}


	/**
	 * @return the currentCity
	 */
	public String getCurrentCity() {
		return currentCity;
	}


	/**
	 * @param currentCity the currentCity to set
	 */
	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}


	/**
	 * @return the currentTownOrArea
	 */
	public String getCurrentTownOrArea() {
		return currentTownOrArea;
	}


	/**
	 * @param currentTownOrArea the currentTownOrArea to set
	 */
	public void setCurrentTownOrArea(String currentTownOrArea) {
		this.currentTownOrArea = currentTownOrArea;
	}


	/**
	 * @return the currentZipCode
	 */
	public String getCurrentZipCode() {
		return currentZipCode;
	}


	/**
	 * @param currentZipCode the currentZipCode to set
	 */
	public void setCurrentZipCode(String currentZipCode) {
		this.currentZipCode = currentZipCode;
	}


	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}


	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}


	/**
	 * @return the nativeState
	 */
	public String getNativeState() {
		return nativeState;
	}


	/**
	 * @param nativeState the nativeState to set
	 */
	public void setNativeState(String nativeState) {
		this.nativeState = nativeState;
	}


	/**
	 * @return the nativeCity
	 */
	public String getNativeCity() {
		return nativeCity;
	}


	/**
	 * @param nativeCity the nativeCity to set
	 */
	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}


	/**
	 * @return the nativeTownOrArea
	 */
	public String getNativeTownOrArea() {
		return nativeTownOrArea;
	}


	/**
	 * @param nativeTownOrArea the nativeTownOrArea to set
	 */
	public void setNativeTownOrArea(String nativeTownOrArea) {
		this.nativeTownOrArea = nativeTownOrArea;
	}


	/**
	 * @return the nativeZipCode
	 */
	public String getNativeZipCode() {
		return nativeZipCode;
	}

 
	/**
	 * @param nativeZipCode the nativeZipCode to set
	 */
	public void setNativeZipCode(String nativeZipCode) {
		this.nativeZipCode = nativeZipCode;
	}


	/**
	 * @return the nativeAddress
	 */
	public String getNativeAddress() {
		return nativeAddress;
	}


	/**
	 * @param nativeAddress the nativeAddress to set
	 */
	public void setNativeAddress(String nativeAddress) {
		this.nativeAddress = nativeAddress;
	}
	
	


	/**
	 * @return the nativeLatitude
	 */
	public double getNativeLatitude() {
		return nativeLatitude;
	}


	/**
	 * @param nativeLatitude the nativeLatitude to set
	 */
	public void setNativeLatitude(double nativeLatitude) {
		this.nativeLatitude = nativeLatitude;
	}


	/**
	 * @return the nativeLongitude
	 */
	public double getNativeLongitude() {
		return nativeLongitude;
	}


	/**
	 * @param nativeLongitude the nativeLongitude to set
	 */
	public void setNativeLongitude(double nativeLongitude) {
		this.nativeLongitude = nativeLongitude;
	}


	@Override
	public String toString() {
		String str = "";
		
		try {
			str = new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
				
	}
	
}
