package com.ansitech.findmate.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.ansitech.findmate.entity.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RequirementModel {

	private int requirmentsId;
	private String currentState;
	private String currentCity;
	private String nativeState;
	private String nativeCity;
	private String currentAddress;
	private Double budget;
	private String size;
	private String tenantType;
	private boolean isActive=true;
	private String landmark;
	private String sharing;
	private int noOfTenantsRequired;
	private UserModel user;
	private List<FlatePhotosModel> flatImages;	
	private String requirementType;   // flat requirement or flat mate requirement
	
	/**
	 * @return the requirmentsId
	 */
	public int getRequirmentsId() {
		return requirmentsId;
	}
	/**
	 * @param requirmentsId the requirmentsId to set
	 */
	public void setRequirmentsId(int requirmentsId) {
		this.requirmentsId = requirmentsId;
	}
	/**
	 * @return the currentState
	 */
	public String getCurrentState() {
		return currentState;
	}
	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	/**
	 * @return the currentCity
	 */
	public String getCurrentCity() {
		return currentCity;
	}
	/**
	 * @param currentCity the currentCity to set
	 */
	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}
	/**
	 * @return the nativeState
	 */
	public String getNativeState() {
		return nativeState;
	}
	/**
	 * @param nativeState the nativeState to set
	 */
	public void setNativeState(String nativeState) {
		this.nativeState = nativeState;
	}
	/**
	 * @return the nativeCity
	 */
	public String getNativeCity() {
		return nativeCity;
	}
	/**
	 * @param nativeCity the nativeCity to set
	 */
	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}
	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}
	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}
	/**
	 * @return the budget
	 */
	public Double getBudget() {
		return budget;
	}
	/**
	 * @param budget the budget to set
	 */
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the tenantType
	 */
	public String getTenantType() {
		return tenantType;
	}
	/**
	 * @param tenantType the tenantType to set
	 */
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	/**
	 * @return the user
	 */
	public UserModel getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(UserModel user) {
		this.user = user;
	}
	

	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	
	/**
	 * @return the sharing
	 */
	public String getSharing() {
		return sharing;
	}
	/**
	 * @param sharing the sharing to set
	 */
	public void setSharing(String sharing) {
		this.sharing = sharing;
	}
	
	
	
	/**
	 * @return the flatImages
	 */
	public List<FlatePhotosModel> getFlatImages() {
		return flatImages;
	}
	/**
	 * @param flatImages the flatImages to set
	 */
	public void setFlatImages(List<FlatePhotosModel> flatImages) {
		this.flatImages = flatImages;
	}
	
	/**
	 * @return the noOfTenantsRequired
	 */
	public int getNoOfTenantsRequired() {
		return noOfTenantsRequired;
	}
	/**
	 * @param noOfTenantsRequired the noOfTenantsRequired to set
	 */
	public void setNoOfTenantsRequired(int noOfTenantsRequired) {
		this.noOfTenantsRequired = noOfTenantsRequired;
	}
	/**
	 * @return the requirementType
	 */
	public String getRequirementType() {
		return requirementType;
	}
	/**
	 * @param requirementType the requirementType to set
	 */
	public void setRequirementType(String requirementType) {
		this.requirementType = requirementType;
	}
	
	
	@Override
	public String toString() {
		
		String str = null;
		
		try {
			str = new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return str;
		
	}
}
