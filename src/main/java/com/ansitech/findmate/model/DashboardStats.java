package com.ansitech.findmate.model;

public class DashboardStats {
	
	private int totalActiveUsers;
	private int totalActiveProperty;
	private int totalActiveRequirement;
	private int todaysRegistration;
	/**
	 * @return the totalActiveUsers
	 */
	public int getTotalActiveUsers() {
		return totalActiveUsers;
	}
	/**
	 * @param totalActiveUsers the totalActiveUsers to set
	 */
	public void setTotalActiveUsers(int totalActiveUsers) {
		this.totalActiveUsers = totalActiveUsers;
	}
	/**
	 * @return the totalActiveProperty
	 */
	public int getTotalActiveProperty() {
		return totalActiveProperty;
	}
	/**
	 * @param totalActiveProperty the totalActiveProperty to set
	 */
	public void setTotalActiveProperty(int totalActiveProperty) {
		this.totalActiveProperty = totalActiveProperty;
	}
	/**
	 * @return the totalActiveRequirement
	 */
	public int getTotalActiveRequirement() {
		return totalActiveRequirement;
	}
	/**
	 * @param totalActiveRequirement the totalActiveRequirement to set
	 */
	public void setTotalActiveRequirement(int totalActiveRequirement) {
		this.totalActiveRequirement = totalActiveRequirement;
	}
	/**
	 * @return the todaysRegistration
	 */
	public int getTodaysRegistration() {
		return todaysRegistration;
	}
	/**
	 * @param todaysRegistration the todaysRegistration to set
	 */
	public void setTodaysRegistration(int todaysRegistration) {
		this.todaysRegistration = todaysRegistration;
	}
	
	
}
