package com.ansitech.findmate.model;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PropertyModel {

	private int id;
	private String state;
	private String city;
	private String streetAddress;
	private String landMark;
	private String nativeState;
	private String nativeCity;
	private String tenantType;
	private boolean isActive=true;
	private String propertyType;
	private String availableRooms;
	private String furnishedType;
	private boolean isBalconyAvailable;
	private boolean isSeperateWashroom;
	private UserModel user;
	private List<FacilityModel> facilities;
	private List<FlatePhotosModel> flatImages;	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}



	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}



	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}



	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}



	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}



	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}



	/**
	 * @return the landMark
	 */
	public String getLandMark() {
		return landMark;
	}



	/**
	 * @param landMark the landMark to set
	 */
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}



	/**
	 * @return the nativeState
	 */
	public String getNativeState() {
		return nativeState;
	}



	/**
	 * @param nativeState the nativeState to set
	 */
	public void setNativeState(String nativeState) {
		this.nativeState = nativeState;
	}



	/**
	 * @return the nativeCity
	 */
	public String getNativeCity() {
		return nativeCity;
	}



	/**
	 * @param nativeCity the nativeCity to set
	 */
	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}



	/**
	 * @return the tenantType
	 */
	public String getTenantType() {
		return tenantType;
	}



	/**
	 * @param tenantType the tenantType to set
	 */
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}



	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}



	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}



	/**
	 * @return the propertyType
	 */
	public String getPropertyType() {
		return propertyType;
	}



	/**
	 * @param propertyType the propertyType to set
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}



	/**
	 * @return the availableRooms
	 */
	public String getAvailableRooms() {
		return availableRooms;
	}



	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(String availableRooms) {
		this.availableRooms = availableRooms;
	}



	/**
	 * @return the furnishedType
	 */
	public String getFurnishedType() {
		return furnishedType;
	}



	/**
	 * @param furnishedType the furnishedType to set
	 */
	public void setFurnishedType(String furnishedType) {
		this.furnishedType = furnishedType;
	}



	/**
	 * @return the isBalconyAvailable
	 */
	public boolean isBalconyAvailable() {
		return isBalconyAvailable;
	}



	/**
	 * @param isBalconyAvailable the isBalconyAvailable to set
	 */
	public void setBalconyAvailable(boolean isBalconyAvailable) {
		this.isBalconyAvailable = isBalconyAvailable;
	}



	/**
	 * @return the isSeperateWashroom
	 */
	public boolean isSeperateWashroom() {
		return isSeperateWashroom;
	}



	/**
	 * @param isSeperateWashroom the isSeperateWashroom to set
	 */
	public void setSeperateWashroom(boolean isSeperateWashroom) {
		this.isSeperateWashroom = isSeperateWashroom;
	}



	/**
	 * @return the user
	 */
	public UserModel getUser() {
		return user;
	}



	/**
	 * @param user the user to set
	 */
	public void setUser(UserModel user) {
		this.user = user;
	}



	/**
	 * @return the facilities
	 */
	public List<FacilityModel> getFacilities() {
		return facilities;
	}



	/**
	 * @param facilities the facilities to set
	 */
	public void setFacilities(List<FacilityModel> facilities) {
		this.facilities = facilities;
	}

	



	/**
	 * @return the flatImages
	 */
	public List<FlatePhotosModel> getFlatImages() {
		return flatImages;
	}



	/**
	 * @param flatImages the flatImages to set
	 */
	public void setFlatImages(List<FlatePhotosModel> flatImages) {
		this.flatImages = flatImages;
	}



	@Override
	public String toString() {
		
		String str = null;
		
		try {
			str = new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return str;
		
	}
}
