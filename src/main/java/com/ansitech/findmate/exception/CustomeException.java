package com.ansitech.findmate.exception;

public class CustomeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public String errorMessage;


	public CustomeException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	

	
}
