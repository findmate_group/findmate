package com.ansitech.findmate.exception;

import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.ansitech.findmate.utils.ErrorDetails;
import com.ansitech.findmate.utils.StringConstants;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
	private static final Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
	
	@ExceptionHandler(InvalidUsernameAndPassword.class)
	public final ResponseEntity<ErrorDetails> handleInvalidUser(InvalidUsernameAndPassword ex, WebRequest web){
		logger.info("entered into exception handler class");
		HashMap<String, String> map = new HashMap<>();
	    map.put("data", ex.getMessage());
	    ErrorDetails details = new ErrorDetails(new Date(),HttpStatus.BAD_REQUEST, StringConstants.UNSUCCESSFUL, map);
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
		
	}
	
	
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<ErrorDetails> noRecordFound(UserNotFoundException ex, WebRequest web){
		logger.info("entered into noRecordFound exception handler class");
		HashMap<String, String> map = new HashMap<>();
	    map.put(StringConstants.DESCRIPTION, ex.getMessage());
	    ErrorDetails details = new ErrorDetails(new Date(),HttpStatus.BAD_REQUEST, StringConstants.UNSUCCESSFUL, map);
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
		
	}
	
	
	@ExceptionHandler(PropertyNotFoundException.class)
	public final ResponseEntity<ErrorDetails> propertyDoesNotAvailable(PropertyNotFoundException ex , WebRequest request){
		
		logger.info("entered into propertyDoesNotAvailable exception handler class");
		HashMap<String, String> map = new HashMap<>();
	    map.put(StringConstants.DESCRIPTION, ex.getMessage());
	    ErrorDetails details = new ErrorDetails(new Date(),HttpStatus.BAD_REQUEST, StringConstants.UNSUCCESSFUL, map);
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
		
	}
	
	
	@ExceptionHandler(NoMatchedRequiredFoundException.class)
	public final ResponseEntity<ErrorDetails> noMatchFound(NoMatchedRequiredFoundException ex , WebRequest request){
		
		logger.info("entered into noMatchFound exception handler class");
		HashMap<String, String> map = new HashMap<>();
	    map.put(StringConstants.DESCRIPTION, ex.getMessage());
	    ErrorDetails details = new ErrorDetails(new Date(),HttpStatus.BAD_REQUEST, StringConstants.UNSUCCESSFUL, map);
		return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
		
	}
	
	
	
	
	
	

}
