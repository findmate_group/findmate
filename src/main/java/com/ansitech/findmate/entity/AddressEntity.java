package com.ansitech.findmate.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name="tbl_address")
public class AddressEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="address_id")
	private int id;
	
	@Column(name="current_state")
	private String currentState;
	
	@Column(name="current_city")
	private String currentCity;
	
	@Column(name="current_town_area")
	private String currentTownOrArea;
	
	@Column(name="current_zipcode")
	private String currentZipCode;
	
	@Column(name="current_address")
	private String currentAddress;
	
	@Column(name="native_state")
	private String nativeState;
	
	@Column(name="native_city")
	private String nativeCity;
	
	@Column(name="native_town_area")
	private String nativeTownOrArea;
	
	@Column(name="native_zipcode")
	private String nativeZipCode;
	
	@Column(name="native_address")
	private String nativeAddress;
	
	@Column(name="n_latitude")
	private double nativeLatitude;
	
	@Column(name="n_longitude")
	private double nativeLongitude;
	
	
	//@OneToOne(fetch = FetchType.LAZY , mappedBy = "address" , cascade = CascadeType.ALL)
	//private UserEntity user;
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the currentState
	 */
	public String getCurrentState() {
		return currentState;
	}



	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}



	/**
	 * @return the currentCity
	 */
	public String getCurrentCity() {
		return currentCity;
	}



	/**
	 * @param currentCity the currentCity to set
	 */
	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}



	/**
	 * @return the currentTownOrArea
	 */
	public String getCurrentTownOrArea() {
		return currentTownOrArea;
	}



	/**
	 * @param currentTownOrArea the currentTownOrArea to set
	 */
	public void setCurrentTownOrArea(String currentTownOrArea) {
		this.currentTownOrArea = currentTownOrArea;
	}



	/**
	 * @return the currentZipCode
	 */
	public String getCurrentZipCode() {
		return currentZipCode;
	}



	/**
	 * @param currentZipCode the currentZipCode to set
	 */
	public void setCurrentZipCode(String currentZipCode) {
		this.currentZipCode = currentZipCode;
	}



	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}



	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}



	/**
	 * @return the nativeState
	 */
	public String getNativeState() {
		return nativeState;
	}



	/**
	 * @param nativeState the nativeState to set
	 */
	public void setNativeState(String nativeState) {
		this.nativeState = nativeState;
	}



	/**
	 * @return the nativeCity
	 */
	public String getNativeCity() {
		return nativeCity;
	}



	/**
	 * @param nativeCity the nativeCity to set
	 */
	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}



	/**
	 * @return the nativeTownOrArea
	 */
	public String getNativeTownOrArea() {
		return nativeTownOrArea;
	}



	/**
	 * @param nativeTownOrArea the nativeTownOrArea to set
	 */
	public void setNativeTownOrArea(String nativeTownOrArea) {
		this.nativeTownOrArea = nativeTownOrArea;
	}



	/**
	 * @return the nativeZipCode
	 */
	public String getNativeZipCode() {
		return nativeZipCode;
	}



	/**
	 * @param nativeZipCode the nativeZipCode to set
	 */
	public void setNativeZipCode(String nativeZipCode) {
		this.nativeZipCode = nativeZipCode;
	}



	/**
	 * @return the nativeAddress
	 */
	public String getNativeAddress() {
		return nativeAddress;
	}



	/**
	 * @param nativeAddress the nativeAddress to set
	 */
	public void setNativeAddress(String nativeAddress) {
		this.nativeAddress = nativeAddress;
	}


	/**
	 * @return the nativeLatitude
	 */
	public double getNativeLatitude() {
		return nativeLatitude;
	}



	/**
	 * @param nativeLatitude the nativeLatitude to set
	 */
	public void setNativeLatitude(double nativeLatitude) {
		this.nativeLatitude = nativeLatitude;
	}



	/**
	 * @return the nativeLongitude
	 */
	public double getNativeLongitude() {
		return nativeLongitude;
	}



	/**
	 * @param nativeLongitude the nativeLongitude to set
	 */
	public void setNativeLongitude(double nativeLongitude) {
		this.nativeLongitude = nativeLongitude;
	}



//	@Override
//	public String toString() {
//		String str = "";
//		
//		try {
//			str = new ObjectMapper().writeValueAsString(this);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		return str;
//				
//	}
}
