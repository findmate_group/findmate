package com.ansitech.findmate.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="tbl_flat_images")
@Entity
public class ImageEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="image_uid")
	private String imageUid;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="req_id")
	private RequirementsEntity requirement;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the imageUid
	 */
	public String getImageUid() {
		return imageUid;
	}

	/**
	 * @param imageUid the imageUid to set
	 */
	public void setImageUid(String imageUid) {
		this.imageUid = imageUid;
	}

	/**
	 * @return the requirement
	 */
	public RequirementsEntity getRequirement() {
		return requirement;
	}

	/**
	 * @param requirement the requirement to set
	 */
	public void setRequirement(RequirementsEntity requirement) {
		this.requirement = requirement;
	}

	
	
}
