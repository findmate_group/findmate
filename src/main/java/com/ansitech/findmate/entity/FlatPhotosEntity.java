package com.ansitech.findmate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Table(name = "tbl_flat_photos")
@Entity
public class FlatPhotosEntity extends Auditable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="imageType")
	private String imageType;
	
	@Column(name="image" , columnDefinition = "text")
	private String image;
	
	@ManyToOne
	private PropertyEntity property;
	
	@ManyToOne
	private RequirementsEntity requirement;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	/**
	 * @return the property
	 */
	public PropertyEntity getProperty() {
		return property;
	}

	/**
	 * @param property the property to set
	 */
	public void setProperty(PropertyEntity property) {
		this.property = property;
	}

	
//	@Override
//	public String toString() {
//
//		String str = null;
//
//		try {
//			str = new ObjectMapper().writeValueAsString(this);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		return str;
//
//	}

}
