package com.ansitech.findmate.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Table(name="tbl_property")
@Entity
public class PropertyEntity extends Auditable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;


	@Column(name="state")
	private String state;

	@Column(name="city")
	private String city;

	@Column(name="street_address")
	private String streetAddress;

	@Column(name="landmark")
	private String landMark;

	@Column(name="tenant_type")
	private String tenantType;

	@Column(name="is_active")
	private boolean isActive = true;

	@Column(name="property_type")
	private String propertyType;

	@Column(name="available_rooms")
	private String availableRooms;
	
	@Column(name="furnished_type")
	private String furnishedType;
	
	@Column(name="is_balcony_available")
	private boolean isBalconyAvailable;
	
	@Column(name="is_seperate_washroom_available")
	private boolean isSeperateWashroom=false;

	@ManyToOne(cascade = CascadeType.MERGE ,fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@OneToMany(cascade = CascadeType.ALL ,  fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	private List<FacilityEntity> facilities;
	
	@OneToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
	@JoinColumn(name="image_id")
	private Set<FlatPhotosEntity> flatImages;
	


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}


	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}


	/**
	 * @return the landMark
	 */
	public String getLandMark() {
		return landMark;
	}


	/**
	 * @param landMark the landMark to set
	 */
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}


	/**
	 * @return the tenantType
	 */
	public String getTenantType() {
		return tenantType;
	}


	/**
	 * @param tenantType the tenantType to set
	 */
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}


	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}


	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	/**
	 * @return the propertyType
	 */
	public String getPropertyType() {
		return propertyType;
	}


	/**
	 * @param propertyType the propertyType to set
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}


	/**
	 * @return the availableRooms
	 */
	public String getAvailableRooms() {
		return availableRooms;
	}


	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(String availableRooms) {
		this.availableRooms = availableRooms;
	}


	/**
	 * @return the furnishedType
	 */
	public String getFurnishedType() {
		return furnishedType;
	}


	/**
	 * @param furnishedType the furnishedType to set
	 */
	public void setFurnishedType(String furnishedType) {
		this.furnishedType = furnishedType;
	}


	/**
	 * @return the isBalconyAvailable
	 */
	public boolean isBalconyAvailable() {
		return isBalconyAvailable;
	}


	/**
	 * @param isBalconyAvailable the isBalconyAvailable to set
	 */
	public void setBalconyAvailable(boolean isBalconyAvailable) {
		this.isBalconyAvailable = isBalconyAvailable;
	}


	/**
	 * @return the isSeperateWashroom
	 */
	public boolean isSeperateWashroom() {
		return isSeperateWashroom;
	}


	/**
	 * @param isSeperateWashroom the isSeperateWashroom to set
	 */
	public void setSeperateWashroom(boolean isSeperateWashroom) {
		this.isSeperateWashroom = isSeperateWashroom;
	}


	/**
	 * @return the user
	 */
	public UserEntity getUser() {
		return user;
	}


	/**
	 * @param user the user to set
	 */
	public void setUser(UserEntity user) {
		this.user = user;
	}


	/**
	 * @return the facilities
	 */
	public List<FacilityEntity> getFacilities() {
		return facilities;
	}


	/**
	 * @param facilities the facilities to set
	 */
	public void setFacilities(List<FacilityEntity> facilities) {
		this.facilities = facilities;
	}
	
	


	/**
	 * @return the flatImages
	 */
	public Set<FlatPhotosEntity> getFlatImages() {
		return flatImages;
	}


	/**
	 * @param flatImages the flatImages to set
	 */
	public void setFlatImages(Set<FlatPhotosEntity> flatImages) {
		this.flatImages = flatImages;
	}


//	@Override
//	public String toString() {
//
//		String str = null;
//
//		try {
//			str = new ObjectMapper().writeValueAsString(this);
//		} catch (JsonProcessingException e) {
//			e.printStackTrace();
//		}
//		return str;
//
//	}


}
