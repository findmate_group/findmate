package com.ansitech.findmate.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Entity
@Table(name = "tbl_requirement")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NamedQueries({
	@NamedQuery(name="RequirementsEntity.getLastTwoRequirement",query = "Select r from RequirementsEntity  r  join r.user u   where u.id =:id  order by r.createdDate desc "),
	@NamedQuery(name="RequirementsEntity.getRequirementByBudget", query = "SELECT r from RequirementsEntity  r WHERE r.isActive=true and r.budget BETWEEN :initialBudget AND :finalBudget"),
	@NamedQuery(name="RequirementEntity.getFlatesRequirement",query ="SELECT r from RequirementsEntity  r WHERE r.requirementType=:requirementType")
})
public class RequirementsEntity extends Auditable implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3693722716292194842L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="current_State")
	private String currentState;
	
	@Column(name="current_City")
	private String currentCity;
	
	@Column(name="native_State")
	private String nativeState;
	
	@Column(name="native_City")
	private String nativeCity;
	
	@Column(name="current_Address")
	private String currentAddress;
	
	@Column(name="budget")
	private Double budget;
	
	@Column(name="size")
	private String size;
	
	@Column(name="tenant_Type")
	private String tenantType;
	
	@Column(name="landmark")
	private String landmark;
	
	@Column(name="is_active")
	private boolean isActive=true;
	
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@OneToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
	@JoinColumn(name="requirement_id")
	private Set<FlatPhotosEntity> flatImages;
	
	@Column(name="requirement_type")
	private String requirementType;
	
	@Column(name="tenant_required")
	private int noOfTenantsRequired;
	
	@Column(name="sharing")
	private String sharing;
	
	
	/**
	 * @return the requirmentsId
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param requirmentsId the requirmentsId to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the currentState
	 */
	public String getCurrentState() {
		return currentState;
	}
	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	/**
	 * @return the currentCity
	 */
	public String getCurrentCity() {
		return currentCity;
	}
	/**
	 * @param currentCity the currentCity to set
	 */
	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}
	/**
	 * @return the nativeState
	 */
	public String getNativeState() {
		return nativeState;
	}
	/**
	 * @param nativeState the nativeState to set
	 */
	public void setNativeState(String nativeState) {
		this.nativeState = nativeState;
	}
	/**
	 * @return the nativeCity
	 */
	public String getNativeCity() {
		return nativeCity;
	}
	/**
	 * @param nativeCity the nativeCity to set
	 */
	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}
	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}
	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}
	/**
	 * @return the budget
	 */
	public Double getBudget() {
		return budget;
	}
	/**
	 * @param budget the budget to set
	 */
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the tenantType
	 */
	public String getTenantType() {
		return tenantType;
	}
	/**
	 * @param tenantType the tenantType to set
	 */
	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}
	
	
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the user
	 */
	public UserEntity getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(UserEntity user) {
		this.user = user;
	}
	/**
	 * @return the landmark
	 */
	public String getLandmark() {
		return landmark;
	}
	/**
	 * @param landmark the landmark to set
	 */
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	

	/**
	 * @return the flatImages
	 */
	public Set<FlatPhotosEntity> getFlatImages() {
		return flatImages;
	}
	/**
	 * @param flatImages the flatImages to set
	 */
	public void setFlatImages(Set<FlatPhotosEntity> flatImages) {
		this.flatImages = flatImages;
	}
	/**
	 * @return the requirementType
	 */
	public String getRequirementType() {
		return requirementType;
	}
	/**
	 * @param requirementType the requirementType to set
	 */
	public void setRequirementType(String requirementType) {
		this.requirementType = requirementType;
	}

	/**
	 * @return the noOfTenantsRequired
	 */
	public int getNoOfTenantsRequired() {
		return noOfTenantsRequired;
	}
	/**
	 * @param noOfTenantsRequired the noOfTenantsRequired to set
	 */
	public void setNoOfTenantsRequired(int noOfTenantsRequired) {
		this.noOfTenantsRequired = noOfTenantsRequired;
	}
	/**
	 * @return the sharing
	 */
	public String getSharing() {
		return sharing;
	}
	/**
	 * @param sharing the sharing to set
	 */
	public void setSharing(String sharing) {
		this.sharing = sharing;
	}
	@Override
	public String toString() {
		String str=null;
		try {
			str=new ObjectMapper().writeValueAsString(this);
		}catch(JsonProcessingException e) {
			e.printStackTrace();
		}
		return str;
	}

}
