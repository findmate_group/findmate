package com.ansitech.findmate.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Aspect
@Component
public class LoggingAspect {


	private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);
	
	@Before(value = "execution(* com.ansitech.findmate.controller.*.*(..))")
	public void logAllMethodsBeforeExcecution(JoinPoint point) {
		
		logger.info("enetered  into ::"+point.getSignature().getName()+" of controller class");
		
		Object[] signatureArgs = point.getArgs();
		
		 ObjectMapper mapper = new ObjectMapper();
         mapper.enable(SerializationFeature.INDENT_OUTPUT);
         try {

             if (signatureArgs[0] != null) {
            	 logger.info("\nRequest object: \n" + mapper.writeValueAsString(signatureArgs[0]));
             }
         } catch (Exception e) {
        	 logger.info("something went wrong ::"+e.getMessage());
         }
         
	}
	
	
	@Before(value = "execution(* com.ansitech.findmate.service.impl.*.*(..))")
	public void printLogInService(JoinPoint point) {
		
		logger.info("enetered  into ::"+point.getSignature().getName()+" of service class");
		
	}
}
