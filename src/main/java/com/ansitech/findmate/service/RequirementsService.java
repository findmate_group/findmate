package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.model.RequirementModel;
import com.ansitech.findmate.utils.ResponseJson;

public interface RequirementsService {
	public ResponseJson<HttpStatus, Object> createRequirement(RequirementModel requirement);

	public ResponseJson<HttpStatus, Object> matchRequirement(int UserId);

	public ResponseJson<HttpStatus, Object> getLastTwoRequirement(Integer userId);

	public ResponseJson<HttpStatus, Object> getRequirementsByBudget(String budget);

	public ResponseJson<HttpStatus, Object> getFlatesRequirementList(String flatType);
}
