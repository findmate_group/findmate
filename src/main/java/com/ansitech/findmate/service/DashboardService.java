package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.utils.ResponseJson;

public interface DashboardService {

	public	ResponseJson<HttpStatus, Object> getDashboardStats();

}
