package com.ansitech.findmate.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ansitech.findmate.model.DashboardStats;
import com.ansitech.findmate.repository.PropertyRepository;
import com.ansitech.findmate.repository.RequirementsRepository;
import com.ansitech.findmate.repository.UserRepository;
import com.ansitech.findmate.service.DashboardService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@Service
public class DashboardServiceImpl implements DashboardService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RequirementsRepository requirementRepo;
	
	@Autowired
	PropertyRepository propertyRepo;
	
	@Override
	public ResponseJson<HttpStatus, Object> getDashboardStats() {
		
	ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		
		HashMap<String, String> map = new HashMap<>();
		
		boolean isActive = true;
		
		int totalActiveUsers = userRepo.countByisActive(isActive);
		
		int totalActivePropery = propertyRepo.countByisActive(isActive);
		
		int totalActiveRequirement = requirementRepo.countByisActive(isActive);
		
		
		if(totalActiveUsers>0 || totalActivePropery>0 || totalActiveRequirement>0) {
			
			DashboardStats dashboardStats = new DashboardStats();
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setStatus(HttpStatus.OK);
			dashboardStats.setTotalActiveUsers(totalActiveUsers);
			dashboardStats.setTotalActiveProperty(totalActivePropery);
			dashboardStats.setTotalActiveRequirement(totalActiveRequirement);
			responseJson.setData(dashboardStats);
			//map.put("totalActiveUsers", String.valueOf(totalActiveUsers));
		}
		
		return responseJson;
	}

}
