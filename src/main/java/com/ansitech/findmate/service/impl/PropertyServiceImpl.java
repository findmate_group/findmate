package com.ansitech.findmate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ansitech.findmate.entity.PropertyEntity;
import com.ansitech.findmate.exception.PropertyNotFoundException;
import com.ansitech.findmate.model.FlatePhotosModel;
import com.ansitech.findmate.model.PropertyModel;
import com.ansitech.findmate.repository.PropertyRepository;
import com.ansitech.findmate.service.PropertyService;
import com.ansitech.findmate.utils.ImageUtil_Old;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@Service
public class PropertyServiceImpl implements PropertyService{

	@Autowired
	PropertyRepository propertyRepo;

	@Autowired
	ModelMapper mapper;

	@Value("${flat.images}")
	private String imagePath;



	@Override
	public ResponseJson<HttpStatus, Object> createProperty(PropertyModel property) {
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		PropertyEntity propertyEntity = null;
		HashMap<String, String> map = new HashMap<>();
		List<FlatePhotosModel> flatPhotosList = new ArrayList<FlatePhotosModel>();
		
		if(property!=null) {

			/**
			 *   convert base64 data to image and save into directory
			 */

			ImageUtil_Old imageUtil = new ImageUtil_Old();
			for(int i=0;i<property.getFlatImages().size();i++) {
				String uidOfImage = UUID.randomUUID().toString();
				FlatePhotosModel flatPhotos = property.getFlatImages().get(i);
				imageUtil.createInputImage(property.getFlatImages().get(i), uidOfImage ,imagePath);
				flatPhotos.setImage(uidOfImage);
			}
			
			propertyEntity = mapper.map(property, PropertyEntity.class);

		//	propertyRepo.save(propertyEntity);
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.PROPERTY_CREATED);
			responseJson.setData(map);
		}

		return responseJson;
	}


	@Override
	public ResponseJson<HttpStatus, Object> propertyList() {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();

		List<PropertyEntity> propertyList = (List<PropertyEntity>) propertyRepo.findAll();

		if(propertyList.size()>0) {
			List<PropertyModel> properties = propertyList.stream().map(obj -> mapper.map(obj, PropertyModel.class)).collect(Collectors.toList());

			if(properties.size()>0) {
				responseJson.setStatus(HttpStatus.OK);
				responseJson.setMessage(StringConstants.SUCCESSFUL);
				responseJson.setData(properties);
			}
		}else {
			throw new PropertyNotFoundException("Property  Not Found");
		}
		return responseJson;
	}

}
