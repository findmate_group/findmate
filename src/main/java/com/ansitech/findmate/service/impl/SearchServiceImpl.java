package com.ansitech.findmate.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ansitech.findmate.entity.PropertyEntity;
import com.ansitech.findmate.repository.PropertyRepository;
import com.ansitech.findmate.service.SearchService;
import com.ansitech.findmate.utils.ResponseJson;

@Service
public class SearchServiceImpl implements SearchService{
	
	@Autowired
	private PropertyRepository propertyRepos;
	
	private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);


	@Override
	public  ResponseJson<HttpStatus, Object> searchFlats(String state, String city) {
		
		ResponseJson<HttpStatus, Object> response = new ResponseJson<HttpStatus, Object>();
		
		String stateValue = state.trim();
		
		String cityValue = city.trim();
		
		List<PropertyEntity> propertyList = propertyRepos.findByStateAndCity(stateValue, cityValue);
		
		logger.info("list ::"+propertyList);
		
		return response;
	}

}
