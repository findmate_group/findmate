package com.ansitech.findmate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

import com.ansitech.findmate.entity.FlatPhotosEntity;
import com.ansitech.findmate.entity.PropertyEntity;
import com.ansitech.findmate.entity.RequirementsEntity;
import com.ansitech.findmate.exception.NoMatchedRequiredFoundException;
import com.ansitech.findmate.model.FlatePhotosModel;
import com.ansitech.findmate.model.MatchedRequirementModel;
import com.ansitech.findmate.model.PropertyModel;
import com.ansitech.findmate.model.RequirementModel;
import com.ansitech.findmate.repository.PropertyRepository;
import com.ansitech.findmate.repository.RequirementsRepository;
import com.ansitech.findmate.service.RequirementsService;
import com.ansitech.findmate.utils.ImageUtil_Old;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

/**
 * 
 * @author ankur shakya
 * @since 31-07-2020
 */
@Service
public class RequirementsServiceImp implements RequirementsService{

	private static final Logger logger = LoggerFactory.getLogger(RequirementsServiceImp.class);

	@Autowired
	RequirementsRepository requirementsRepos;

	@Autowired
	PropertyRepository propertyRepo;

	@Autowired
	ModelMapper mapper;
	
	@Value("${flat.images}")
	private String imagePath;


	@Override
	public ResponseJson<HttpStatus, Object> createRequirement(RequirementModel requirement) {
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		
		RequirementsEntity requirementsEntity = null;

		if(requirement!=null) {
			
			//convert base 64 into images
			
			ImageUtil_Old imageUtil = new ImageUtil_Old();
			for(int i=0;i<requirement.getFlatImages().size();i++) {
				String uidOfImage = UUID.randomUUID().toString();
				FlatePhotosModel flatPhotos = requirement.getFlatImages().get(i);
				imageUtil.createInputImage(flatPhotos, uidOfImage ,imagePath);
				flatPhotos.setImage(uidOfImage);
			}
			
			requirementsEntity = mapper.map(requirement, RequirementsEntity.class);
			requirementsRepos.save(requirementsEntity);
			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, StringConstants.CREATED);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setData(map);

		}else {
			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, StringConstants.FACILITY_EXISTS);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			responseJson.setData(map);

		}

		return responseJson;
	}

	@Override
	public ResponseJson<HttpStatus, Object> matchRequirement(int userId) {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		

		List<RequirementsEntity> allRequirementsFromDb = null;

		List<PropertyEntity>  allActivePropList = new ArrayList();

		List<RequirementModel> otherRequirements = new ArrayList<>();

		List<MatchedRequirementModel> matchedProfiles = new ArrayList<>();

		RequirementModel selfRequirement = new RequirementModel();

		if(userId!=0) {

			boolean isActive = true;

			allRequirementsFromDb = requirementsRepos.findMatchedRequirement(isActive);

			allActivePropList    = propertyRepo.findActiveProperty(isActive);

			logger.info("active requirement list ::"+allRequirementsFromDb.size());

			logger.info("active all property list ::"+allActivePropList.size());

			if(allRequirementsFromDb.size()>0) {
				
				for(int i=0;i<allRequirementsFromDb.size();i++) {

					if(allRequirementsFromDb.get(i).getUser().getId()==userId) {

						RequirementsEntity requirementEntity = allRequirementsFromDb.get(i);

						selfRequirement = mapper.map(requirementEntity, RequirementModel.class);


					}else if(allRequirementsFromDb.get(i).getUser().getId()!=userId) {

						RequirementsEntity requirementEntity = allRequirementsFromDb.get(i);

						RequirementModel requirementModel = mapper.map(requirementEntity, RequirementModel.class);

						otherRequirements.add(requirementModel);
					}
				}

				for(int i=0;i<otherRequirements.size();i++) {

					MatchedRequirementModel matchedRequirement = new MatchedRequirementModel();

					if (StringUtils.equalsIgnoreCase(selfRequirement.getCurrentState(), otherRequirements.get(i).getCurrentState()) 
							&& StringUtils.equalsIgnoreCase(selfRequirement.getCurrentCity(), otherRequirements.get(i).getCurrentCity())
							&& (StringUtils.equalsIgnoreCase(selfRequirement.getTenantType(),otherRequirements.get(i).getTenantType()))
							|| ( (selfRequirement.getBudget()-1500)<=otherRequirements.get(i).getBudget())) {

						matchedRequirement.setId(otherRequirements.get(i).getRequirmentsId());
						matchedRequirement.setName(otherRequirements.get(i).getUser().getFirstName()+" "+otherRequirements.get(i).getUser().getLastName());
						matchedRequirement.setMobileNo(otherRequirements.get(i).getUser().getMobileNo());
						matchedRequirement.setBudget(String.valueOf(otherRequirements.get(i).getBudget()));
						matchedRequirement.setSize(otherRequirements.get(i).getSize());
						matchedRequirement.setTenantType(otherRequirements.get(i).getTenantType());
						matchedRequirement.setAddress(otherRequirements.get(i).getUser().getAddress().getCurrentAddress());
						matchedRequirement.setCity(otherRequirements.get(i).getCurrentCity());

						matchedProfiles.add(matchedRequirement);

					}

				}
			}else if(allActivePropList.size()>0) {

				List<PropertyModel> propertyList = new ArrayList();

				for(int i=0;i<allActivePropList.size();i++) {

					if(allActivePropList.get(i).getUser().getId()!=userId) {

						PropertyModel propertyModel = mapper.map(allActivePropList.get(i), PropertyModel.class);

						propertyList.add(propertyModel);

					}

				}

				for(int i=0;i<propertyList.size();i++) {

					MatchedRequirementModel matchedRequirement = new MatchedRequirementModel();

					if (StringUtils.equalsIgnoreCase(selfRequirement.getCurrentState(), propertyList.get(i).getState()) 
							&& StringUtils.equalsIgnoreCase(selfRequirement.getCurrentCity(), propertyList.get(i).getCity())
							&& (StringUtils.equalsIgnoreCase(selfRequirement.getTenantType(),propertyList.get(i).getTenantType()))) {

						matchedRequirement.setId(otherRequirements.get(i).getRequirmentsId());
						matchedRequirement.setName(otherRequirements.get(i).getUser().getFirstName()+" "+otherRequirements.get(i).getUser().getLastName());
						matchedRequirement.setMobileNo(otherRequirements.get(i).getUser().getMobileNo());
						matchedRequirement.setBudget(String.valueOf(otherRequirements.get(i).getBudget()));
						matchedRequirement.setSize(otherRequirements.get(i).getSize());
						matchedRequirement.setTenantType(otherRequirements.get(i).getTenantType());
						matchedRequirement.setAddress(otherRequirements.get(i).getUser().getAddress().getCurrentAddress());
						matchedRequirement.setCity(otherRequirements.get(i).getCurrentCity());

						matchedProfiles.add(matchedRequirement);

					}
				}
			}

			else {
				logger.info("no match found");
				throw new NoMatchedRequiredFoundException("No Match Found");
			}

			if(matchedProfiles.size()>0) {
				responseJson.setData(matchedProfiles);
				responseJson.setStatus(HttpStatus.OK);
				responseJson.setMessage(StringConstants.SUCCESSFUL);
			}else {
				throw new NoMatchedRequiredFoundException("No Match Found");
			}


			logger.info("data from db for active user  "+ allRequirementsFromDb.size());
		}

		return responseJson;
	}

	@Override
	public ResponseJson<HttpStatus, Object> getLastTwoRequirement(Integer userId) {
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		

		List<RequirementsEntity> reqList = requirementsRepos.getLastTwoRequirement(userId);
		
		reqList = reqList.stream().limit(2).collect(Collectors.toList());
		
		if(reqList.size()>0) {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setData(reqList);
		}else {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, "No Record Found");
			responseJson.setData(map);
		}
		
		logger.info("reqList :: "+reqList.size());
		
		
		return responseJson;
	}

	@Override
	public ResponseJson<HttpStatus, Object> getRequirementsByBudget(String budget) {
		
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		

		 String[] budgetArray = budget.split("to");
		 double initialBudget = Double.parseDouble(budgetArray[0]);
		 double finalBudget = Double.parseDouble(budgetArray[1]);
		 
		 
		List<RequirementsEntity> reqList = requirementsRepos.getRequirementByBudget(initialBudget,finalBudget);
		
		reqList = reqList.stream().limit(2).collect(Collectors.toList());
		
		if(reqList.size()>0) {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setData(reqList);
		}else {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, "No Record Found");
			responseJson.setData(map);
		}
		
		logger.info("reqList :: "+reqList.size());
		
		
		return responseJson;
	}

	@Override
	public ResponseJson<HttpStatus, Object> getFlatesRequirementList(String requirementType) {
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		
        ImageUtil_Old imaheUtil = new ImageUtil_Old();
		
		 
		 
		List<RequirementsEntity> reqList = requirementsRepos.getFlatesRequirementByRequirementType(requirementType);
		
		
		for(int i=0;i<reqList.size();i++) {
			
			Set<FlatPhotosEntity> flatPhotosSet = reqList.get(i).getFlatImages();
			
			for(FlatPhotosEntity flat : flatPhotosSet) {
				
				String base64Image = imaheUtil.getImageByUuid(flat.getImage(), imagePath);
				
				flat.setImage(base64Image);
				
			}
		}
		
		reqList = reqList.stream().limit(2).collect(Collectors.toList());
		
		if(reqList.size()>0) {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setData(reqList);
		}else {
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, "No Record Found");
			responseJson.setData(map);
		}
		
		logger.info("reqList :: "+reqList.size());
		
		
		return responseJson;
	}


}


