package com.ansitech.findmate.service.impl;

import java.util.HashMap;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ansitech.findmate.entity.FacilityEntity;
import com.ansitech.findmate.model.FacilityModel;
import com.ansitech.findmate.repository.FacilityRepository;
import com.ansitech.findmate.service.FacilityService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@Service
public class FacilityServiceImpl implements FacilityService{

	@Autowired
	FacilityRepository facilityRepo;

	@Autowired
	ModelMapper mapper;
	@Override
	public ResponseJson<HttpStatus, Object> createFacility(FacilityModel facility) {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();		
		FacilityEntity facilityEntity = null;

		if(facility!=null && !isExists(facility.getFacilityType())) {
			facilityEntity = mapper.map(facility, FacilityEntity.class);

			facilityRepo.save(facilityEntity);

			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, StringConstants.SUCCESS_FACILITY);
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setData(map);

		}else {
			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, StringConstants.FACILITY_EXISTS);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			responseJson.setData(map);

		}

		return responseJson;
	}
	private boolean isExists(String facilityType) {
		boolean isExists = false;

		FacilityEntity facility = facilityRepo.findByFacilityType(facilityType);

		if(facility!=null) {
			isExists = true;
		}
		return isExists;
	}

}
