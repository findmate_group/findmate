package com.ansitech.findmate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ansitech.findmate.entity.UserEntity;
import com.ansitech.findmate.exception.CustomeException;
import com.ansitech.findmate.exception.InvalidUsernameAndPassword;
import com.ansitech.findmate.exception.UserNotFoundException;
import com.ansitech.findmate.model.UserModel;
import com.ansitech.findmate.repository.UserRepository;
import com.ansitech.findmate.service.UserService;
import com.ansitech.findmate.utils.ImageUtil_Old;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@Service
public class UserServiceImpl implements UserService{

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);


	@Autowired
	ModelMapper mapper;

	@Value("${user.image.path}")
	private String imagePath;

	@Autowired
	UserRepository userRepo;

	@Override
	public ResponseJson<HttpStatus, Object> saveUser(UserModel user) throws CustomeException{

		logger.info("entered into save user");
		UserEntity userEntity = null;

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();

		if(user!=null) {
			try {
				userEntity = mapper.map(user, UserEntity.class);

			}
			catch(MappingException e) {

				throw new CustomeException("Mapping from dto to entity failed");

			}

			if(userEntity != null) {
				if(!isUserExists(user.getMobileNo(),user.getEmailId())) {
					UserEntity userFmDb = userRepo.save(userEntity);	
					
					logger.info("Userfm db "+userFmDb);
					if(userFmDb.getEmailId()!=null) {
						logger.info("user is saved successfully");
						HashMap<String, String> map = new HashMap<>();
						responseJson.setStatus(HttpStatus.OK);
						responseJson.setMessage(StringConstants.SUCCESSFUL);
						map.put(StringConstants.DESCRIPTION,StringConstants.SUCCESS_REGISTER);
						responseJson.setData(map);
					}

				}else {

					logger.info("user already exists");

					throw new CustomeException(StringConstants.USER_EXISTS);
				}


			}
			
			logger.info("response to client ::"+responseJson);
		}
		return responseJson;
	}


	private boolean isUserExists(String mobileNo, String emailId) {

		boolean isUserExists = false;
		UserEntity userDb = null;

		userDb = userRepo.findByMobileNoOrEmailId(mobileNo,emailId);

		if(userDb!=null) {
			isUserExists = true;
		}
		return isUserExists;

	}


	@Override
	public ResponseJson<HttpStatus, Object> loginUser(UserModel user) {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, Object> map = new HashMap<String, Object>();
		UserEntity userDb = null;
		if(StringUtils.isNotBlank(user.getEmailId()) && StringUtils.isNotBlank(user.getPassword())) {

			String emailId = user.getEmailId();
			String password = user.getPassword();

			userDb = userRepo.findByEmailIdAndPassword(emailId, password);

			if(userDb!=null) {
				responseJson.setStatus(HttpStatus.OK);
				responseJson.setMessage(StringConstants.SUCCESSFUL);
				map.put(StringConstants.DESCRIPTION, StringConstants.LOGIN_SUCCESS);
				map.put("data", userDb);
				responseJson.setData(map);
			}else {

				throw new InvalidUsernameAndPassword(StringConstants.INVALID_USER_PASSWORD);
			}
		}

		return responseJson;
	}


	@Override
	public ResponseJson<HttpStatus, Object> updateUser(UserModel user) {
		UserEntity userEntity = null;
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();

		if(user!=null) {
			UserEntity userFmDb = null;
			try {
				userEntity = mapper.map(user, UserEntity.class);

			}
			catch(MappingException e) {
				throw new CustomeException("Mapping from dto to entity failed");
			}


			if(userEntity != null) {

				/**
				 *   convert base64 data to image and save into directory
				 */
				ImageUtil_Old imageUtil = new ImageUtil_Old();
				//imageUtil.convertBase64ToImage(userEntity.getPhoto(), userEntity.getId(),imagePath);


				if(!isUserExists(user.getMobileNo(),user.getEmailId()) || StringUtils.isNotBlank(String.valueOf(user.getId()))) {
					userFmDb = userRepo.save(userEntity);	
					if(userFmDb!=null) {
						logger.info("user is updated successfully");
						HashMap<String, String> map = new HashMap<>();
						responseJson.setStatus(HttpStatus.OK);
						responseJson.setMessage(StringConstants.SUCCESSFUL);
						map.put(StringConstants.DESCRIPTION,StringConstants.SUCCESS_UPDATE);
						responseJson.setData(map);
					}

				}else {

					logger.info("user record not updated something went wrong");

					throw new CustomeException(StringConstants.USER_EXISTS);
				}

			}
		}
		return responseJson;
	}


	@Override
	public ResponseJson<HttpStatus, Object> getAllUsers() {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();

		List<UserModel> listUsers = null;

		boolean isActive = true;
		List<UserEntity> listUserDb = (List<UserEntity>) userRepo.findByIsActive(isActive);

		if(listUserDb.isEmpty()) {
			logger.info("No Records Found");
			throw new UserNotFoundException("No Record Found");
		}else {

			listUsers = listUserDb.stream().map(obj-> mapper.map(obj, UserModel.class)).collect(Collectors.toList());
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setStatus(HttpStatus.OK);
			responseJson.setData(listUsers);
		}

		return responseJson;
	}


	@Override
	public ResponseJson<HttpStatus, Object> deActivateUser(int id) {

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();
		HashMap<String, String> map = new HashMap<>();
		int updatedId = userRepo.deActiveUser(id);

		if(updatedId!=0) {
			logger.info("User DeActivated");
			responseJson.setMessage(StringConstants.SUCCESSFUL);
			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, StringConstants.DEACTIVATED_SUCCESS);
			responseJson.setData(map);
		}else {
			logger.info("User is not de Activated");
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			responseJson.setStatus(HttpStatus.OK);
			map.put(StringConstants.DESCRIPTION, "User Not DeActivated");
			responseJson.setData(map);
		}


		return responseJson;
	}


	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "Good boy";
	}


	

}
