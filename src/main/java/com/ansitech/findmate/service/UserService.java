package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.exception.CustomeException;
import com.ansitech.findmate.model.UserModel;
import com.ansitech.findmate.utils.ResponseJson;

public interface UserService {
	
	public ResponseJson<HttpStatus, Object> saveUser(UserModel user)throws CustomeException;

	public ResponseJson<HttpStatus, Object> loginUser(UserModel user);

	public ResponseJson<HttpStatus, Object> updateUser(UserModel user)throws CustomeException;

	public ResponseJson<HttpStatus, Object> getAllUsers();

	public ResponseJson<HttpStatus, Object> deActivateUser(int id);
	
	public String getMessage();


}
