package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.model.PropertyModel;
import com.ansitech.findmate.utils.ResponseJson;

public interface PropertyService {

	public ResponseJson<HttpStatus, Object> createProperty(PropertyModel property);

	public ResponseJson<HttpStatus, Object> propertyList();
}
