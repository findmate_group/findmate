package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.model.FacilityModel;
import com.ansitech.findmate.utils.ResponseJson;

public interface FacilityService {

	
	public ResponseJson<HttpStatus, Object> createFacility(FacilityModel facility);
}
