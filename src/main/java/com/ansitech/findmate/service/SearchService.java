package com.ansitech.findmate.service;

import org.springframework.http.HttpStatus;

import com.ansitech.findmate.entity.PropertyEntity;
import com.ansitech.findmate.utils.ResponseJson;

public interface SearchService {
	public  ResponseJson<HttpStatus, Object> searchFlats(String state,String city);

}
