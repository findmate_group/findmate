package com.ansitech.findmate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.ansitech.findmate.entity.PropertyEntity;

@Repository
public interface PropertyRepository extends CrudRepository<PropertyEntity, Integer> {

	@Query(value = "select * from tbl_property as r inner join tbl_user as u on r.user_id = u.user_id  where is_active=?", nativeQuery = true)
	public List<PropertyEntity> findActiveProperty(boolean isActive);

	
	public List<PropertyEntity> findByStateAndCity(@Param("state") String state, @Param("city") String city);
	
	public int countByisActive(@Param("isActive") boolean isActive);

}
