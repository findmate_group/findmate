package com.ansitech.findmate.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ansitech.findmate.entity.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer>{

    	
	
	public UserEntity findByMobileNoOrEmailId(String mobileNo,String emailId);
	
	public UserEntity findByEmailIdAndPassword(String emailId , String password);
	
	
	public List<UserEntity> findByIsActive(@Param("isActive") boolean isActive);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE UserEntity SET isActive=false where id=:id")
	public int deActiveUser(@Param("id") int id);
	
	
	public int countByisActive(@Param("isActive") boolean isActive);
	

	
}
