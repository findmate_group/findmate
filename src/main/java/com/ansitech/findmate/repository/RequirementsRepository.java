package com.ansitech.findmate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ansitech.findmate.entity.RequirementsEntity;

@Repository
public interface RequirementsRepository extends CrudRepository<RequirementsEntity, Integer> {

	@Query(value = "select * from tbl_requirement as r inner join tbl_user as u on r.user_id = u.user_id  where is_active=?" ,nativeQuery = true)
	public List<RequirementsEntity>  findMatchedRequirement(boolean isActive);

	public int countByisActive(@Param("isActive") boolean isActive);
	
	public List<RequirementsEntity> getLastTwoRequirement(Integer id);
	
	
	public List<RequirementsEntity> getRequirementByBudget(double initialBudget , double finalBudget);
	
	public List<RequirementsEntity> getFlatesRequirementByRequirementType(String requirementType);
	
	

}
