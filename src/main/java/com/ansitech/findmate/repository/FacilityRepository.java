package com.ansitech.findmate.repository;
import org.springframework.data.repository.CrudRepository;

import com.ansitech.findmate.entity.FacilityEntity;

public interface FacilityRepository extends CrudRepository<FacilityEntity, Integer>{

	public FacilityEntity findByFacilityType(String type);
}
