package com.ansitech.findmate.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ansitech.findmate.exception.CustomeException;

/**
 * The Class ImageUtil.
 */
@Component
public class ImageUtil {

	/** The logger. */
	private static final Log logger = LogFactory.getLog(ImageUtil.class);

	/** The image util instance. */
	private static ImageUtil imageUtilInstance;

	/**
	 * Instantiates a new image util.
	 */
	public ImageUtil() {
	}

	/**
	 * Gets the single instance of ImageUtil.
	 *
	 * @return single instance of ImageUtil
	 */
	public static ImageUtil getInstance() {

		if (imageUtilInstance == null) {
			imageUtilInstance = new ImageUtil();
		}

		return imageUtilInstance;
	}

	/**
	 * Convert base 64 to image.
	 *
	 * @param base64String
	 *            the base 64 string
	 * @param imageType
	 *            the image type
	 * @param imageFile
	 *            the image file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void convertBase64ToImage(String base64String, String imageType, File imageFile) throws IOException {

		//logger.info("Converting base64 data to image" + imageFile.getName());

		byte[] imgBytes = Base64.getDecoder().decode(base64String);

		BufferedImage img = ImageIO.read(new ByteArrayInputStream(imgBytes));
		if (img != null) {
			ImageIO.write(img, imageType, imageFile);
		}
	}

	public boolean createDirectory(String directoryName) {

		File file = new File(directoryName);

		if (!file.exists()) {

			return file.mkdirs();

		}

		return true;

	}

	public Boolean createImage(String uuid, String imageData, String fileName,
			Environment environment) throws CustomeException {

		//logger.info("Creating image**********************************"+fileName);

		String[] ids = StringUtils.split(uuid, "-");
		String imageUploadDirLocation = environment.getProperty("assets.base.path") + File.separator + "images"
				+ File.separator + StringUtils.join(ids, File.separator);

		boolean isDirectoryExist = ImageUtil.getInstance().createDirectory(imageUploadDirLocation);

		if (!isDirectoryExist) {
			logger.info("Failed to create directory");
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));
		}

		boolean isImageCreated = false;

		try {

			File imageFile = new File(imageUploadDirLocation + File.separator + fileName+".png");
			ImageUtil imageUtil = ImageUtil.getInstance();

			if (imageData != null) {
				imageUtil.convertBase64ToImage(imageData, "png", imageFile);
			}
			isImageCreated = true;

		} catch (IOException ioe) {

			logger.error("Exception occured while uploading/updating user profile photo.", ioe);
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));

		}

		return isImageCreated;
	}




	public Boolean createInputImage(String uuid, String imageData, String fileName,
			Environment environment) throws CustomeException {

		//logger.info("Creating image**********************************"+fileName);

		String[] ids = StringUtils.split(uuid, "-");
		String imageUploadDirLocation = environment.getProperty("assets.base.path") + File.separator + "userphoto"
				+ File.separator + StringUtils.join(ids, File.separator);

		boolean isDirectoryExist = ImageUtil.getInstance().createDirectory(imageUploadDirLocation);

		if (!isDirectoryExist) {
			logger.info("Failed to create directory");
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));
		}

		boolean isImageCreated = false;

		try {

			File imageFile = new File(imageUploadDirLocation + File.separator + fileName+".png");
			ImageUtil imageUtil = ImageUtil.getInstance();

			if (imageData != null) {
				imageUtil.convertBase64ToImage(imageData, "png", imageFile);
			}
			isImageCreated = true;

		} catch (IOException ioe) {

			logger.error("Exception occured while uploading/updating user profile photo.", ioe);
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));

		}

		return isImageCreated;
	}


	public Boolean createUserSignatureImage(String uuid, String imageData, String fileName,
			Environment environment) throws CustomeException {

		//	logger.info("Creating image**********************************"+fileName);

		String[] ids = StringUtils.split(uuid, "-");
		String imageUploadDirLocation = environment.getProperty("assets.base.sign.path") + File.separator + "user_signature"
				+ File.separator + StringUtils.join(ids, File.separator);

		boolean isDirectoryExist = ImageUtil.getInstance().createDirectory(imageUploadDirLocation);

		if (!isDirectoryExist) {
			logger.info("Failed to create directory");
			//	throw new CustomException(messageService.getMessage("message.runtime.exception"));
		}

		boolean isImageCreated = false;

		try {

			File imageFile = new File(imageUploadDirLocation + File.separator + fileName+".png");
			ImageUtil imageUtil = ImageUtil.getInstance();

			if (imageData != null) {
				imageUtil.convertBase64ToImage(imageData, "png", imageFile);
			}
			isImageCreated = true;

		} catch (IOException ioe) {

			logger.error("Exception occured while uploading/updating user profile photo.", ioe);
			//	throw new CustomException(messageService.getMessage("message.runtime.exception"));

		}

		return isImageCreated;
	}


	public String resizeImage(String base64string,String fileName,String imagesPath) {

		if (StringUtils.isBlank(base64string)) {
			return "";
		}

		logger.info("Entered into resizeImage with filename::"+fileName);
		byte[] imgBytes = null;
		if(StringUtils.equalsIgnoreCase(fileName, "resizePhoto1.png")) {
			imgBytes = DatatypeConverter.parseBase64Binary(base64string.split(",")[1]); 
		} else {
			imgBytes = DatatypeConverter.parseBase64Binary(base64string);
		}

		/*
		 * newly added code to set pixels of photo
		 */
		BufferedImage image;
		FileInputStream fileInputStreamReader = null;
		File file = null;
		BufferedImage outputImage = null;
		String encodedfile = "";
		try {
			image = ImageIO.read(new ByteArrayInputStream(imgBytes));

			int type = image.getType() == 0? BufferedImage.TYPE_INT_ARGB : image.getType();
			outputImage = new BufferedImage(150,150, type);
			Graphics2D g2d = outputImage.createGraphics();
			g2d.setComposite(AlphaComposite.Src);
			g2d.drawImage(image, 0, 0,150,150, null);
			g2d.dispose();
			file = new File(imagesPath + fileName);
			ImageIO.write(outputImage, "png",file);


			fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int)file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = Base64.getEncoder().encodeToString(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fileInputStreamReader!=null) {
				try {
					fileInputStreamReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		logger.info("Leaving from resizeImage method");
		return encodedfile;
	}


	public Boolean createInputImageForResize(String uuid, String imageData, String fileName, 
			Environment environment) throws CustomeException {

		//logger.info("Creating image**********************************"+fileName);

		String[] ids = StringUtils.split(uuid, "-");
		String imageUploadDirLocation = environment.getProperty("assets.base.sign.path.resize") + File.separator + "resizeImages"
				+ File.separator + StringUtils.join(ids, File.separator);

		boolean isDirectoryExist = ImageUtil.getInstance().createDirectory(imageUploadDirLocation);

		if (!isDirectoryExist) {
			logger.info("Failed to create directory");
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));
		}

		boolean isImageCreated = false;

		try {

			File imageFile = new File(imageUploadDirLocation + File.separator + fileName+".png");
			ImageUtil imageUtil = ImageUtil.getInstance();

			if (imageData != null) {
				imageUtil.convertBase64ToImage(imageData, "png", imageFile);
			}
			isImageCreated = true;

		} catch (IOException ioe) {

			logger.error("Exception occured while uploading/updating user profile photo.", ioe);
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));

		}

		return isImageCreated;
	}




}
