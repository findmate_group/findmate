package com.ansitech.findmate.utils;

import java.util.Date;
import java.util.HashMap;

import org.springframework.http.HttpStatus;

public class ErrorDetails {

	private Date timestamp;
	private HttpStatus status;
	private String message;
	private HashMap<String, String> data;
	
	
	public ErrorDetails(Date timestamp, HttpStatus status,String message, HashMap<String, String> data) {
		super();
		this.timestamp = timestamp;
		this.status = status;
		this.message = message;
		this.data = data;
	}


	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}


	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	
	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the data
	 */
	public HashMap<String, String> getData() {
		return data;
	}


	/**
	 * @param data the data to set
	 */
	public void setData(HashMap<String, String> data) {
		this.data = data;
	}


	@Override
	public String toString() {
		return "ErrorDetails [timestamp=" + timestamp + ", message=" + message + ", data=" + data + "]";
	}
	
	
	
	
}
