package com.ansitech.findmate.utils;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

import com.ansitech.findmate.exception.CustomeException;
import com.ansitech.findmate.model.FlatePhotosModel;


public class ImageUtil_Old {


	public void convertBase64ToImage(String base64Image,String uidOfImage,String imagePath) {

		decoder(base64Image, imagePath+"\\"+uidOfImage+".png");


	} 

//		public static String encoder(String imagePath) {
//		    String base64Image = "";
//		    File file = new File(imagePath);
//		    try (FileInputStream imageInFile = new FileInputStream(file)) {
//		      // Reading a Image file from file system
//		      byte imageData[] = new byte[(int) file.length()];
//		      imageInFile.read(imageData);
//		      base64Image = Base64.getEncoder().encodeToString(imageData);
//		    } catch (FileNotFoundException e) {
//		      System.out.println("Image not found" + e);
//		    } catch (IOException ioe) {
//		      System.out.println("Exception while reading the Image " + ioe);
//		    }
//		    return base64Image;
//		  }

	public static void decoder(String base64Image, String pathFile) {

		File file = new File(pathFile);
		if(!file.exists()) {
			file.getParentFile().mkdir();
		}
		try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
			// Converting a Base64 String into Image byte array

			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
		} catch (FileNotFoundException e) {
		} catch (IOException ioe) {
		}
	}
	
	

	public Boolean createInputImage(FlatePhotosModel flatePhotosModel, String imageUid, String basePath) throws CustomeException {

		//logger.info("Creating image**********************************"+fileName);

		String[] ids = StringUtils.split(imageUid, "-");
		String imageUploadDirLocation = basePath + File.separator +  File.separator + StringUtils.join(ids, File.separator);

		boolean isDirectoryExist = ImageUtil.getInstance().createDirectory(imageUploadDirLocation);

		if (!isDirectoryExist) {
			//logger.info("Failed to create directory");
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));
		}

		boolean isImageCreated = false;

		try {

			File imageFile = new File(imageUploadDirLocation + File.separator + "room"+".png");
			ImageUtil imageUtil = ImageUtil.getInstance();

			if (flatePhotosModel != null) {
				imageUtil.convertBase64ToImage(flatePhotosModel.getImage(), "png", imageFile);
			}
			isImageCreated = true;

		} catch (IOException ioe) {

		//	logger.error("Exception occured while uploading/updating user profile photo.", ioe);
			//throw new CustomException(messageService.getMessage("message.runtime.exception"));

		}

		return isImageCreated;
	}
	
	public String getImageByUuid(String uuid, String folder) throws CustomeException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Map<String,String> base64StringMap = new HashMap<>();

		String[] ids = StringUtils.split(uuid, "-");
	//	String directoryLocation = environment.getProperty("assets.base.path");

		String imageDirLocation = folder + File.separator
				+ StringUtils.join(ids, File.separator);

				try {
					Files.list(Paths.get(imageDirLocation + "/")).filter(Files::isRegularFile)
							.forEach(new FunctionExceptionWrapper().exceptionWrapper(path -> {
								String fileExt = "png";
								BufferedImage image = ImageIO.read(path.toFile());
								ImageIO.write(image, fileExt, baos);
								baos.flush();
								Base64.getEncoder().encodeToString(baos.toByteArray());
								base64StringMap.put("image", Base64.getEncoder().encodeToString(baos.toByteArray()));
		
								baos.close();
		
							}, IOException.class));
		
				} catch (IOException e) {
		           e.printStackTrace();
					//logger.error("Exception occured while retrieving  photo.");
					throw new CustomeException("Exception occured while retrieving  photo.");
				}
				
		return (String) base64StringMap.get("image");

	}

}
