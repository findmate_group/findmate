package com.ansitech.findmate.utils;

public class StringConstants {

	public static final String SUCCESSFUL = "Successful";
	
	public static final String DESCRIPTION = "description";
	
	public static final String SUCCESS_REGISTER = "User Registered Successfully";

	public static final String SUCCESS_FACILITY = "Facility Registered Successfully";

	
	public static final String USER_EXISTS = "User Already Exists";
	
	public static final String FACILITY_EXISTS = "Facility Already Exists";
	
	
	public static final String LOGIN_SUCCESS = "User LoggedIn Successfully";
	
	public static final String INVALID_USER_PASSWORD = "Invalid username and password";
	
	public static final String SUCCESS_UPDATE = "User Updated Successfully";
	
	public static final String UNSUCCESSFUL = "Unsuccessful";
	
	public static final String PROPERTY_CREATED = "Property Created Successfully";
	
	public static final String CREATED = "Successfully Created";
	
	public static final String DEACTIVATED_SUCCESS = "User  Deactivated Successfully";
	
	public static final String CONTACT_ADMIN = "Something went wrong please contact to admin";
	
	
	
}
