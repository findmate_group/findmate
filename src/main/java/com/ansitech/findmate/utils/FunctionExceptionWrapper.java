package com.ansitech.findmate.utils;

import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FunctionExceptionWrapper {

	/** The logger. */
	private final Log logger = LogFactory.getLog(this.getClass());

	/**
	 * Exception wrapper.
	 *
	 * @param <T> the generic type
	 * @param <E> the element type
	 * @param consumer the consumer
	 * @param exception the exception
	 * @return the consumer
	 */
	public <T, E extends Exception> Consumer<T> exceptionWrapper(ThrowingConsumer<T, Exception> consumer, Class<E> exception) {
			
		return mail -> {
			try {
				consumer.accept(mail);
			} catch (Exception e) {
				try {
					E ex = exception.cast(e);
					logger.info(ex.getMessage());
				} catch (ClassCastException ce) {
					logger.info(ce.getMessage());
				}
			}
		};
	}
}
