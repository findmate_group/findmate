package com.ansitech.findmate.utils;

public class ResponseJson<S,T> {
	
	/** The status. */
	private S status;
	
	/** The message. */
	private String message;
	
	/** The data. */
	private T data;

	/**
	 * @return the status
	 */
	public S getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(S status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}


	

}
