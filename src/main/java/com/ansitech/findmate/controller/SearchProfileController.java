package com.ansitech.findmate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ansitech.findmate.service.SearchService;

@RestController
@RequestMapping("/rest/api")
public class SearchProfileController {

	private static final Logger logger = LoggerFactory.getLogger(SearchProfileController.class);
	
	@Autowired
	private SearchService searchService;

	@GetMapping("/searchflat/{state}/{city}")
	public ResponseEntity<Object> searchFlat(@PathVariable("state") String state , @PathVariable("city") String city) {

		logger.info("enetered into search profile ::" + state +" city "+city);
		searchService.searchFlats(state, city);

		return null;
	}

}
