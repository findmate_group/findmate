package com.ansitech.findmate.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ansitech.findmate.model.PropertyModel;
import com.ansitech.findmate.service.PropertyService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@RestController
@RequestMapping("/rest/api")
public class PropertyController {

	@Autowired
	PropertyService propertyService;
	
	
	@PostMapping("/createproperty")
	public ResponseEntity<Object> createProperty(@RequestBody PropertyModel propertyModel){
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
		try {
		responseJson = propertyService.createProperty(propertyModel);
		}catch(RuntimeException e) {
			HashMap<String, String> map = new HashMap<String, String>();
			
			map.put("description", StringConstants.CONTACT_ADMIN);
			responseJson.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			responseJson.setData(map);
		}
		return new ResponseEntity<>(responseJson, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/listofproperties")
	public ResponseEntity<Object> listOfProperty(){
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
		responseJson = propertyService.propertyList();
		
		return new ResponseEntity<>(responseJson, HttpStatus.OK);
		
	}
	
	@GetMapping("/gettotalactiveproperty")
	public ResponseEntity<?> getTotalActiveProperty(){
		
		return null;
		
	}
	
}
