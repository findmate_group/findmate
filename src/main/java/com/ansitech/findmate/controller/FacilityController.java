package com.ansitech.findmate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ansitech.findmate.model.FacilityModel;
import com.ansitech.findmate.service.FacilityService;
import com.ansitech.findmate.utils.ResponseJson;

@RestController
@RequestMapping("/rest/api")
public class FacilityController {

	@Autowired
	FacilityService facilityService;

	@PostMapping("/createfacility")
	public ResponseEntity<Object> createFacility(@RequestBody FacilityModel facility){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();

		responseJson = facilityService.createFacility(facility);

		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);

	}

}
