package com.ansitech.findmate.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ansitech.findmate.model.RequirementModel;
import com.ansitech.findmate.model.UserModel;
import com.ansitech.findmate.service.RequirementsService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

/**
 * 
 * @author ankur shakya
 * @since 31-07-2020
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/rest/api")
public class RequirementsController {
	
	private static final Logger logger = LoggerFactory.getLogger(RequirementsController.class);
	
	@Autowired
	RequirementsService requirementsService;
	

	
	@PostMapping("/userrequirement")
	@CrossOrigin(origins = "http://localhost:3000/")
	public ResponseEntity<Object> createRequirement(@RequestBody RequirementModel requirement){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();

		try {
		responseJson = requirementsService.createRequirement(requirement);
		}catch(RuntimeException e) {
			logger.info("exception ::"+e.getMessage());
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
		}
		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);

	}

	
	@PostMapping("/matchrequirement")
	public ResponseEntity<Object> getMatchedRequirements(@RequestBody  UserModel user){
		
		logger.info("user id ::"+ user.getId());
		
		//logger.info("profile env is :: "+message);
		
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
		responseJson = requirementsService.matchRequirement(user.getId());

		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);
		
	}
	
	
	@GetMapping("/gettotalactiverequirement")
	public ResponseEntity<?> getTotalActiveRequirement(){
		
		return null;
		
	}
	
	@GetMapping(value="/getLastTwoRequirement")
	@CrossOrigin(origins = "http://localhost:3000/")
	public ResponseEntity<?> getLastTwoRequirement(@RequestParam(value="id",required = false,defaultValue = "1") int id){
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
		//logger.info("entered into getLastTwoRequirement controller ::: with id  "+id);

		try {
		responseJson = requirementsService.getLastTwoRequirement(id);
		}catch(RuntimeException e) {
			logger.info("exception ::"+e.getMessage());
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
		}
		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);

	}
	
	
	@GetMapping(value="/getrequirementbybudget")
	@CrossOrigin(origins = "http://localhost:3000/")
	public ResponseEntity<?> getRequirementsByBudget(@RequestParam(value="budget",required = false,defaultValue = "4500 to 6500") String budget){
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
		logger.info("entered into getRequirementsByBudget controller ::: with budget  "+budget);

		try {
		responseJson = requirementsService.getRequirementsByBudget(budget);
		}catch(RuntimeException e) {
			logger.info("exception ::"+e.getMessage());
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
		}
		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);

	}
	
	
	
	@GetMapping(value="/getFlatesRequirementList")
	@CrossOrigin(origins = "http://localhost:3000/")
	public ResponseEntity<?> getFlatesRequirementList(@RequestParam(value="requirementType",required = false) String requirementType){
		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		
	//	logger.info("entered into getRequirementsByBudget controller ::: with budget  "+budget);

		try {
		responseJson = requirementsService.getFlatesRequirementList(requirementType);
		}catch(RuntimeException e) {
			logger.info("exception ::"+e.getMessage());
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
		}
		return new ResponseEntity<Object>(responseJson, HttpStatus.OK);

	}
}
