package com.ansitech.findmate.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ansitech.findmate.service.DashboardService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@RequestMapping("/rest/api")
@RestController
public class DashboardController {

	@Autowired
	DashboardService dashboardService;

	@GetMapping("/getdashboardstats")
	public ResponseEntity<?> getDashboardStats(){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();

		try {
			responseJson = dashboardService.getDashboardStats();
		}
		catch(RuntimeException e) {
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.BAD_REQUEST);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
			//	logger.info(e.getMessage());

		}

		return new ResponseEntity<>(responseJson, HttpStatus.OK);
	}
}
