package com.ansitech.findmate.controller;

import java.util.HashMap;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ansitech.findmate.exception.CustomeException;
import com.ansitech.findmate.model.UserModel;
import com.ansitech.findmate.service.UserService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("rest/api")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);


	@Autowired
	UserService userService;

	@PostMapping("/createuser")
	@CrossOrigin(origins = "http://localhost:3000/")
	public ResponseEntity<Object> createUser(@RequestBody UserModel user){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();

		try {
			responseJson = userService.saveUser(user);
		}catch(CustomeException e) {
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setMessage("Unsuccessful");
			responseJson.setStatus(HttpStatus.OK);
			map.put("code", "500");
			map.put("description", e.getMessage());
			responseJson.setData(map);
		}

		return new ResponseEntity<>(responseJson, HttpStatus.OK);

	}

	@PostMapping("/updateuser")
	public ResponseEntity<Object> updateUser(@RequestBody UserModel user){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<HttpStatus, Object>();

		try {
			responseJson = userService.updateUser(user);
		}catch(CustomeException e) {
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setMessage("Unsuccessful");
			responseJson.setStatus(HttpStatus.OK);
			map.put("code", "500");
			map.put("description", e.getMessage());
			responseJson.setData(map);
		}

		return new ResponseEntity<>(responseJson, HttpStatus.OK);

	}


	@GetMapping("/listusers")
	public ResponseEntity<Object> getAllUsers(){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();

		responseJson = userService.getAllUsers();

		return new ResponseEntity<>(responseJson, HttpStatus.OK);

	}


	@PostMapping("/loginuser")
	public ResponseEntity<Object> loginUser(@RequestBody UserModel user){

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		responseJson = userService.loginUser(user);
		return new ResponseEntity<>(responseJson, HttpStatus.OK);

	}

	@GetMapping("/deactivateuser/{id}")
	public ResponseEntity<Object> deActiVateUser(@PathVariable("id") int id){

		logger.info("User id :: "+id);

		ResponseJson<HttpStatus, Object> responseJson = new ResponseJson<>();
		try {
			responseJson = userService.deActivateUser(id);
		}
		catch(RuntimeException e) {
			HashMap<String, String> map = new HashMap<String, String>();
			responseJson.setStatus(HttpStatus.BAD_REQUEST);
			responseJson.setMessage(StringConstants.UNSUCCESSFUL);
			map.put(StringConstants.DESCRIPTION, StringConstants.CONTACT_ADMIN);
			responseJson.setData(map);
			logger.info(e.getMessage());

		}

		return new ResponseEntity<>(responseJson, HttpStatus.OK);

	}

}
