package com.ansitech.findmate;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.ansitech.findmate.utils.ImageUtil_Old;

@SpringBootApplication
@EnableJpaAuditing
public class FindmateApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindmateApplication.class, args);
	}
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	
	
	

}
