package com.ansitech.findmate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import com.ansitech.findmate.entity.UserEntity;
import com.ansitech.findmate.model.AddressModel;
import com.ansitech.findmate.model.UserModel;
import com.ansitech.findmate.repository.UserRepository;
import com.ansitech.findmate.service.UserService;
import com.ansitech.findmate.utils.ResponseJson;
import com.ansitech.findmate.utils.StringConstants;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FindmateApplicationTests {

	@Autowired	
	private UserService userService;

	@Mock
	private UserRepository userRepo;

	@Autowired
	ModelMapper mapper;


	public FindmateApplicationTests() {

	}

	@Test
	public void saveUserTest() {
		
		UserModel user = new UserModel();
		user.setFirstName("malal");
		user.setLastName("yadav");
		user.setEmailId("malalayadav@gmail.com");
		user.setMobileNo("82323882818");
		user.setRole("USER");
		AddressModel address = new AddressModel();
		address.setCurrentState("TN");
		address.setCurrentCity("Hyderabad");
		address.setCurrentAddress("Hyderabad");
		user.setAddress(address);

		UserEntity userEntity = mapper.map(user, UserEntity.class);
		
		System.out.println(userEntity);
		ResponseJson<HttpStatus, Object> response = new ResponseJson<HttpStatus, Object>();
		response.setMessage(StringConstants.SUCCESSFUL);
		response.setStatus(HttpStatus.OK);
		HashMap<String, String> map = new HashMap<>();
		map.put(StringConstants.DESCRIPTION, StringConstants.CREATED);
		
		response.setData(map);

	    when(userRepo.save(userEntity)).thenReturn(userEntity);
		assertEquals(response, userService.saveUser(user));
	}
	
	
//	@Test
//	public void getMessageTest() {
//		
//		String message = "Good boy";
//		
//		assertEquals(message, userService.getMessage());
//	}

}
